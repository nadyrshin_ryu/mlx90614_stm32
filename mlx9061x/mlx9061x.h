//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _MLX9061X_H
#define _MLX9061X_H

#include "../types.h"
#include "i2cm.h"


#define MLX9061X_I2C_ADDR       0xB4    // I2C-����� mlx9061x (��-��������� 0xB4)
#define MLX9061X_I2C_CLOCK      100000  // ���������� ��� mlx90614 �������� - �� 10� �� 100�
#define MLX9061X_I2C_TO         1000    // ������� �������� � ���������� i2c (� �����)

// ���������, ������������ ������ ��� ������ RAM �� mlx9061x
#define MLX9061X_ERR_Ok         I2C_ERR_Ok
#define MLX9061X_ERR_NotConnect I2C_ERR_NotConnect
#define MLX9061X_ERR_BadChksum  I2C_ERR_BadChksum
#define MLX9061X_ERR_HWerr      I2C_ERR_HWerr
#define MLX9061X_ERR_MeasErr    -10                     // ������ ��������� �����������



// ��������� ������������� ���������� ������ � mlx9061x
void mlx90614_init(I2C_TypeDef* I2Cx);
// ������� ������ 1 �������� �� mlx9061x
int8_t mlx9061x_ReadReg(I2C_TypeDef* I2Cx, uint8_t slave_addr, uint8_t RamAddr, float *pTemp);

int8_t mlx9061x_ReadTa(I2C_TypeDef* I2Cx, uint8_t slave_addr, float *pTemp);
int8_t mlx9061x_ReadTo1(I2C_TypeDef* I2Cx, uint8_t slave_addr, float *pTemp);
int8_t mlx9061x_ReadTo2(I2C_TypeDef* I2Cx, uint8_t slave_addr, float *pTemp);


#endif